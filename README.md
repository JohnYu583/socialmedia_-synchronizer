1. Open a new terminal and use "mongod" to run mongodb before you run the server.

2. Use "npm install" to install required node modules.

3. Use "node server.js" to run the server.

4. Go to "localhost:8080" to enjoy the services.