// Dependencies
// -----------------------------------------------------
var express         = require('express');
var mongoose        = require('mongoose');
var port            = process.env.PORT || 8080;
var morgan          = require('morgan');
var bodyParser      = require('body-parser');
var methodOverride  = require('method-override');
var Twitter         = require('twitter');
var twitterAPI      = require('node-twitter-api');
var app             = express();

// Set Twitter app information
var client = new Twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY || "YXvUO5rz6nVHVvhH5pEPU32FE",
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET || "hMUBvuRjd0cpltWmK3QT5EsiXueubkCd0awzbH6tGDCXYO1xRp",
  access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY || "717731922403803136-2Fo5lGMwOP9mKCxZpubRx5iMdfGOtXj",
  access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET || "KUC2GUnkORACXxYzVwawlmXJBUVYX4ptH0ZCl4CznlnoA",
});
//twitter auth
var twitter = new twitterAPI({
	consumerKey: 'YXvUO5rz6nVHVvhH5pEPU32FE',
	consumerSecret: 'hMUBvuRjd0cpltWmK3QT5EsiXueubkCd0awzbH6tGDCXYO1xRp',
	callback: 'https://yzhao583-ece9065-finalproject-johnyu583.c9users.io/#/Post'
});

var requesttoken="", requesttoken_secret="",accesstoken="",accesstoken_secret="",verifier="";
// Express Configuration
// -----------------------------------------------------
// Sets the connection to MongoDB
mongoose.connect("mongodb://localhost/finalproject");

// Logging and Parsing
app.use(express.static(__dirname + '/public'));                 // sets the static files location to public
app.use('/bower_components',  express.static(__dirname + '/bower_components')); // Use BowerComponents
//app.use(morgan('dev'));                                         // log with Morgan
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.urlencoded({extended: true}));               // parse application/x-www-form-urlencoded
app.use(bodyParser.text());                                     // allows bodyParser to look at raw text
app.use(bodyParser.json({ type: 'application/vnd.api+json'}));  // parse application/vnd.api+json as json
app.use(methodOverride());

// Router
// ------------------------------------------------------
var router = express.Router();              // get an instance of the express Router
// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// more routes for our API will happen here
router.route('/')

    // create a user (accessed at POST http://localhost:8080/server)
    .post(function(req, res) {
        
        // var user = new User();      // create a new instance of the User model
        // user.name = req.body.name;  // set the user name (comes from the request)
        // user.email = req.body.email; // set the user email (comes from the request)
        // user.gender = req.body.gender; // set the user gender (comes from the request not necessary)
        // user.link = req.body.link; //set the user link (comes from the request not necessary)
        // user.picture = req.body.picture; //set the user picture (comes from the request not necessary)
        // user.locale = req.body.locale; //set the user locale (comes from the request not necessary)
        // user.posts = req.body.posts;
        // user.comments = req.body.comments;
        //Check if user is already exsit
        User.find({'email': req.body.email}, function(err, users){
            if (users.length > 0)
            {
                console.log(users[0].email);
                console.log(users.length + " this is the length of user in MDB");
                //res.json({message: 'User exsits'});
                var user = new User();      // create a new instance of the User model
                user.name = req.body.name||0;  // set the user name (comes from the request)
                user.email = req.body.email||0; // set the user email (comes from the request)
                user.gender = req.body.gender||0; // set the user gender (comes from the request not necessary)
                user.link = req.body.link||0; //set the user link (comes from the request not necessary)
                user.picture = req.body.picture||0; //set the user picture (comes from the request not necessary)
                user.locale = req.body.locale||0; //set the user locale (comes from the request not necessary)
                user.posts = req.body.posts||0;
                user.posts.comments = req.body.posts.comments||0;
                user._id = users[0]._id ? users[0]._id: ObjectID(user_id);// get user_id
                var errm ="";
                //---------------------------------------------------------------------
                // If user exists, update user 
                user.update(user._id,function(err) {
                    errm =err;
                    if(!(err)){
                        res.send("User exsits and updated");
                        console.log("'User exsits and updated");
                       // res.json({message: 'User saved'});
                    }else{
                    res.send("there is an error ,User exists and not updated");
                      console.log(err);
                    }
                });
            }else{ 
                var user = new User();      // create a new instance of the User model
                user.name = req.body.name||0;  // set the user name (comes from the request)
                user.email = req.body.email||0; // set the user email (comes from the request)
                user.gender = req.body.gender||0; // set the user gender (comes from the request not necessary)
                user.link = req.body.link||0; //set the user link (comes from the request not necessary)
                user.picture = req.body.picture||0; //set the user picture (comes from the request not necessary)
                user.locale = req.body.locale||0; //set the user locale (comes from the request not necessary)
                user.posts = req.body.posts||0;
                user.posts.comments = req.body.posts.comments||0;
                // if user is not exist create user (save)
                user.save(function(err) {
                    if(err){
                        res.send(err);
                    }else{
                        res.send("user saved!");
                    }
                    
                });
            }
        });
    })
    // get all the users (accessed at GET http://localhost:8080/server)
    .get(function(req, res) {
        User.find(function(err, users) {
            if (err)
                res.send(err);

            res.json(users);
        });
    })
    .put(function(req, res) {

        // use our user model to find the user we want
        User.findOne({'email': req.body.email}, function(err, user) {

            if (err)
                res.send(err);

            user.posts.content = req.body.posts;  // update the users info

            // save the user
            user.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'user updated!' });
            });

        });
    })
    .delete(function(req, res) {
        User.remove({'email': req.body.email}, function(err, user) {
            if (err)
                res.send(err);
            res.json({ message: 'Successfully deleted' });
        });
    });

// on routes that end in /posts
// ----------------------------------------------------
router.route('/posts')

    .post(function(req, res) {
        //Check if user is already exsit
        User.findOne({'email': req.body.email}, function(err, user){
            user.posts.push(req.body.posts);
            //user.posts.comments.push(req.body.posts.comments);
            user.save(function(err){
                if(err)
                {
                    res.send(err);
                }else{
                    res.send("post created!");
                }
            });
        });
        //Post to Twitter
        client.post('statuses/update', {status: req.body.posts.content},  function(error, tweet, response){
          if(error){
              console.log(error);
          }else{
            console.log(tweet);  // Tweet body. 
            console.log(response);// Raw response object.
          } 
        });
    })
    // get the user with that email 
    .put(function(req, res) {
        console.log("server:" + req.body.email);
        User.findOne({'email': req.body.email}, function(err, user) {
            if (err){
                res.send(err);
            }else if(user.posts.length==0){
                res.send("There is no post");
            }else{
                console.log("retrieve succeed!");
                //console.log(user.posts);
                res.send(user.posts);
            }
        });
        client.get('search/tweets', {q: 'tt'}, function(error, tweets, response){
           console.log(tweets);
        });
    })
    // delete the user with this email 
    .delete(function(req, res) {
        console.log("server:" + req.body);
        User.findOne({'email' : req.body}, function (err, user) {
            console.log(user.email);
            if (!err) {
               var post_num = user.posts.length;
               console.log(post_num);
                user.posts[post_num-1].remove();
                user.save(function (err) {
                    if(err){
                        res.send(err);
                    }else{
                    res.send("post deleted!");
                    }
                });
            }else{
                res.send(err);
                console.log(err);
            }
        });
    });
router.route('/posts/comments')
    .put(function(req, res) {
        console.log("server:" + req.body.email);
        User.findOne({'email': req.body.email}, function(err, user) {
            if (err){
                res.send(err);
            }else if(user.posts.length==0){
                res.send("There is no post");
            }else{
                console.log("retrieve succeed!");
                //console.log(user.posts);
                var posts_num = user.posts.length;
                res.send(user.posts[posts_num-1].content);
            }
        });
    });
router.route('/posts/comments/content')
    .put(function(req, res) {
        console.log("server:" + req.body.email);
        User.findOne({'email': req.body.email}, function(err, user) {
            if (err){
                res.send(err);
            }else if(user.posts.length==0){
                res.send("There is no comment");
            }else{
                console.log("retrieve succeed!");
                //console.log(user.posts);
                res.send(user.posts[0].comments);
            }
        });
    });
router.route('/twitter')
    .post(function(req, res) {
            //get request token and store it
            twitter.getRequestToken(function(error, requestToken, requestTokenSecret, oauth_verifier, results){
            	if (error) {
            		console.log("Error getting OAuth request token : " + error);
            	} else {
            		//store token and tokenSecret somewhere, you'll need them later; redirect user
            		requesttoken=requestToken;
            		console.log(requesttoken);
            		requesttoken_secret=requestTokenSecret;
            		console.log(requesttoken_secret);
            		verifier=oauth_verifier;
            	    console.log(verifier);
            		var url= "https://api.twitter.com/oauth/authenticate?oauth_token="+requestToken;
             		res.contentType('application/json');
                    var data = JSON.stringify(url)
                    res.header('Content-Length', data.length);
                    res.end(data);
            	}
            });
            // get access token
            // twitter.getAccessToken(requesttoken, requesttoken_secret, verifier, function(error, accessToken, accessTokenSecret, results) {
            // 	if (error) {
            // 		//console.log(error);
            // 	} else {
            // 		//store accessToken and accessTokenSecret somewhere (associated to the user) 
            // 		accesstoken=accessToken;
            // 		console.log("twitter accesstoken is"+accesstoken);
            // 		accesstoken_secret=accesstoken_secret;
            // 		console.log("twitter accesstoken_secret is"+accesstoken_secret);
            // 	}
            // });
    });
    
// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /server
app.use('/server', router);
var User = require('./app/model');

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);