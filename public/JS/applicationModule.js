// JavaScript File
/*global angular*/
// create a module for route
var routeApp = angular.module('routeApp', ['ngRoute','ui.bootstrap']);
	// configure our routes
	routeApp.config(function($routeProvider) {
		$routeProvider

			// route for the login page
			.when('/', {
				templateUrl : 'Views/Login.html',
				controller  : 'logcontroller'
			})

			.when('/Login', {
				templateUrl : 'Views/Login.html',
				controller  : 'logcontroller'
			})

			// route for the post page
			.when('/Post', {
				templateUrl : 'Views/Posting.html',
			})
			// route for the track page
			.when('/PostsTracking', {
				templateUrl : 'Views/PostsTracking.html',
			})
			// route for the comment page
			.when('/Comments', {
				templateUrl : 'Views/SpecificPostComments.html',
			});
	});
	