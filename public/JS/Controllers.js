// JavaScript File
/*global angular*/
//collect facebook_accesstoken
// This is called with the results from from FB.getLoginStatus().
    var facebook_accesstoken;
    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
        // Logged into your app and Facebook.
        console.log("access token is:" + response.authResponse.accessToken);
        facebook_accesstoken = response.authResponse.accessToken;
        alert("You have already logged in!");
        } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        // document.getElementById('status').innerHTML = 'Please log ' +
        //   'into Facebook.';
        FB.login(function(response) {
   // handle the response
            console.log(JSON.stringify(response));
            facebook_accesstoken = response.authResponse.accessToken;
        }, {scope: 'publish_actions,user_posts'});
        }
    }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
    function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
    }

   


//Collect google_accesstoken and userinfo, install userinfo in local value
var currentURL=window.location.href;
var token = "mytoken";
//get google_accesstoken
    token=currentURL.split('&').filter(function(element){if(element.match('access_token')!==null)return true;})[0].split('=')[1];
//log google_accesstoken
    console.log("i am the token");
	console.log(token);
	routeApp.run(function($rootScope, $http){
	    $http({
	        method: 'GET',
            url: "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + token
	    }).then(function(response,err){
	        if(err)
	            console.log(err);
	        $rootScope.user = response.data;
	        console.log("login succeed!" +$rootScope.user);
	    });
	    
	});
// // create a controller to logout
    routeApp.controller('logoutcontroller',function($scope,$location,$http){

        //logout function    
        $scope.logout = function(){
        // go to login page
        
            $location.url('/Login')
        };
    
    });
// create a controller for routeApp to control title
    routeApp.controller('titlecontroller', function($scope, $http, $rootScope) {
      //retrieve post title from local
        $http({
            method: 'PUT',
            url: "/server/posts/comments", 
            data: JSON.stringify({email:$rootScope.user.email})
        }).then(function (response,err) {
            console.log("client: email has been sent, and email is" + $rootScope.user.email);
            if(err){
                console.log(err);
            }else if(response.data.length==0){
                alert("There is no post in local!");
            //alert(JSON.stringify(response.data));
            }else{
                 $scope.PostTitle = response.data;
            }
        });
      //retrieve post title from facebook
        $http({
            method: 'GET',
            url: "https://graph.facebook.com/"+$rootScope.facebook_post_id+"?access_token=" + facebook_accesstoken, 
        }).then(function (response,err) {
            if(err){
                console.log(err);
            }else if(response.data.message.length==0){
                alert("There is no post on facebook!");
            }else{
                $scope.PostTitle = response.data.message;
            // alert(response.data.message);
            }
        });
                    
    });
// create a controller to retrieve comments
    routeApp.controller('retrievecommentscontroller', function($scope, $http, $rootScope) {
      //retrieve comments from local
      var local_comments="",facebook_comments="";
      var facebook_comment="";
      var facebook_comments_num;
        $http({
            method: 'PUT',
            url: "/server/posts/comments/content", 
            data: JSON.stringify({email:$rootScope.user.email})
        }).then(function (response,err) {
            console.log("client: email has been sent, and email is" + $rootScope.user.email);
            if(err){
                 console.log(err);
            }else if(response.data.length==0){
                alert("There is no comment in local!");
            }else{
                local_comments = JSON.stringify({"local_comments":response.data});
                //alert(JSON.stringify(response.data));
            }
        });
        //retrieve comments from facebook
        $http({
            method: 'GET',
            url: "https://graph.facebook.com/"+$rootScope.facebook_post_id+"?fields=comments&access_token=" + facebook_accesstoken, 
        }).then(function (response,err) {
            if(err){
                console.log(err);
            }else if(response.data.comments.data.length==0){
                alert("There is no comment from facebook!");
            }else{
                facebook_comments_num = response.data.comments.data.length;
                console.log("facebook comment num is :" + facebook_comments_num);
                for(var i=0;i<facebook_comments_num;i++){
                    facebook_comment = JSON.stringify({"facebook_comments":response.data.comments.data[i]});
                    facebook_comments = facebook_comments + facebook_comment +'\n'
                }
                $scope.comments = local_comments + facebook_comments;
            // alert(JSON.stringify(response));
            }
        });
    });
// create a controller for routeApp to retrieve posts
    routeApp.controller('retrievepostscontroller', function($scope, $http, $rootScope) {
        var local_post="",facebook_post="",twitter_post="",linkedin_post="";
        var local_posts="",facebook_posts="",twitter_posts="",linkedin_posts="";
        var local_posts_num,facebook_posts_num,twitter_posts_num,linkedin_posts_num;
      //retrieve posts from local
        $http({
            method: 'PUT',
            url: "/server/posts", 
            data: JSON.stringify({email:$rootScope.user.email})
        }).then(function (response,err) {
            console.log("client: email has been sent, and email is" + $rootScope.user.email);
            if(err){
                console.log(err);
            }else if(response.data.length==0){
                alert("There is no post in local!");
            }else{
                local_posts_num = response.data.length;
                console.log("local posts num is:"+ local_posts_num);
                for(var i=0;i<local_posts_num;i++){
                    local_post = JSON.stringify({"local_post":response.data[i].content});
                    local_posts = local_posts + local_post +'\n';
                }
                // alert(local_posts);
            }
            //retrieve posts from facebook
            $http({
                method: 'GET',
                url: "https://graph.facebook.com/me?fields=posts&access_token=" + facebook_accesstoken, 
            }).then(function (response,err) {
                if(err){
                    console.log(err);
                }else if(response.data.posts.data.length==0){
                    alert("There is no post on facebook");
                }else{
                    facebook_posts_num = response.data.posts.data.length;
                    console.log("facebook posts num is:"+ facebook_posts_num);
                    for(var i=0;i<facebook_posts_num;i++){
                        facebook_post = JSON.stringify({"facebook_post":response.data.posts.data[i]});
                        facebook_posts = facebook_posts + facebook_post +'\n';
                    }
                    $scope.trackedposts = local_posts + facebook_posts;
                    //alert(JSON.stringify(response.data));
                }
            });
        });
    });
// create a controller for routeApp to control buttons
    routeApp.controller('ButtonsCtrl', function($scope, $http, $rootScope) {
      // the function to create post.
        $scope.createpost = function(){
            // use http service to post a PUT request to update posts
            $http({
                method: 'POST',
                url: "/server/posts", 
                data:JSON.stringify({ 
                    email: $rootScope.user.email, 
                    posts: {content: $scope.posts}
                })
            }).then(function (response,err) {
                if(err)
                    console.log(err);
                    alert("post created!");
            });
            //post on facebook
            $http({
                method: 'POST',
                url: "https://graph.facebook.com/me/feed?access_token=" + facebook_accesstoken, 
                data:JSON.stringify({ 
                    message:$scope.posts
                })
            }).then(function (response,err) {
                if(err){
                    console.log(err);
                }else{
                    alert("post to facebook succeed!");
                    $rootScope.facebook_post_id = response.data.id;
                    console.log($rootScope.facebook_post_id);
                }
            });
        };
        //the function to delete posts;
        $scope.deletepost = function(){
            // use http service to post a PUT request to update posts
            console.log("client:" + $rootScope.user.email);
            $http({
                method: 'DELETE',
                url: "/server/posts", 
                data:(
                    $rootScope.user.email
                )
            }).then(function (response,err) {
                if(err){
                    console.log(err);
                }else{
                    alert(JSON.stringify(response.data));
                }
                $http({
                    method: 'DELETE',
                    url: "https://graph.facebook.com/"+$rootScope.facebook_post_id+"?access_token=" + facebook_accesstoken
                }).then(function(response, err) {
                    if(err){
                        console.log(err);
                    }else{
                        alert("Post was deleted from facebook!");
                    }
                });
            });
        };
    });
   //create a controller for routeApp to login 
    routeApp.controller('logincontroller',function($scope,$location,$http){
        $http({
            method: 'GET',
            url: "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + token
        }).then(function (response) {
            $scope.user = response.data;
            $http({
                method: 'POST',
                url: "/server", 
                data:JSON.stringify({name:$scope.user.name, 
                                    email:$scope.user.email, 
                                    gender:$scope.user.gender, 
                                    link:$scope.user.link,
                                    picture:$scope.user.picture,
                                    locale:$scope.user.locale,
                                    posts: []
                }),
            }).then(function (response) {
                $location.url('/Post');
            });
        });
    });
  //LinkedIn auth controller.
    routeApp.controller('linkincontroller',function($scope,$location,$http){
        $scope.linkinauth = function(){
            $http({
                method: 'GET',
                url: "https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=77q4hmjipytoo6&redirect_uri=https://yzhao583-ece9065-finalproject-johnyu583.c9users.io&state=987654321&scope=r_basicprofile r_emailaddress w_share" 
                // data:(
                //     $rootScope.user.email
                // )
            }).then(function (response,err) {
                if(err){
                    console.log(err);
                }else{
                    alert(JSON.stringify(response));
                }
            });
        };
    });
    //twitter auth controller.
    routeApp.controller('twittercontroller',function($scope,$location,$http){
        $scope.twitterauth = function(){
            $http({
                method: 'POST',
                url: "/server/twitter" 
                // data:(
                //     $rootScope.user.email
                // )
            }).then(function (response,err) {
                if(err){
                    console.log(err);
                }else{
                    alert(response.data);
                    window.open(response.data);
                }
            });
        };
    });
   
	

