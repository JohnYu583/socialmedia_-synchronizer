// Pulls Mongoose dependency for creating schemas
var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

// Creates a User Schema. This will be the basis of how user data is stored in the db
var UserSchema = new Schema({
    name: {type: String, required: false},
    gender: {type: String, required: false},
    email: {type: String, required: true},
    link: {type: String, required: false},
    picture: {type: String, required: false},
    locale: {type: String, required: false}, 
    posts: [{
        content: {type: String, required: false},
        comments: [{
            comment_content:{type: String, required: false}
        }]
    }]
});
// Exports the UserSchema for use elsewhere. Sets the MongoDB collection to be used as: "scotch-users"
module.exports = mongoose.model('User', UserSchema);